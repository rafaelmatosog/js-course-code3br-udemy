# js-course-code3br-udemy

**Link**: [Curso Web Moderno com JavaScript 2019 COMPLETO + Projetos](https://www.udemy.com/course/curso-web/)

**Resumo**: 509 Lições ~ 76,5 horas

**Descrição**: Seja muito bem vindo ao Curso Web Moderno com JavaScript! COMPLETO 2019 + Projetos! Esse que talvez seja o maior e mais completo curso de tecnologia do mercado. São quase 500 aulas divididas em 32 capítulos com mais de 75 horas. Além dos fundamentos e vários exercícios, são vários projetos com as principais tecnologias da atualidade com aplicação na vida real, te ensinando a ser um desenvolvedor Web desde o básico até ao nível profissional avançado...

Desenvolvimento Web é hoje um dos assuntos mais relevantes do momento porque o mercado respira Web. E Se você deseja ser um profissional preparado para o mercado e não quer aprender apenas fórmulas, mas o porquês, esse curso é a escolha certa pra você.

Neste curso iremos abordar o desenvolvimento Web de ponta a ponta, para que você seja capaz de construir uma aplicação com as principais tecnologias do mercado. São 13 cursos dentro de um só. Você irá aprender Javascript, que é hoje a linguagem da atualidade, várias empresas estão migrando suas bases de PHP, Python e outras para terem suas bases completamente feitas em javascript. Também irá aprender Node, os últimos recursos de HTML e CSS, Gulp, Webpack, jQuery, Bootstrap, React ( Tecnologia usada por umas das maiores empresas do mundo, o Facebook), Vue JS, ExpressJS, MySQL e MongoDB.

Nesse curso você também irá aprender os paradigmas de programação usados na Web Moderna: Funcional, Orientação a Objeto e etc. Aplicações Web baseadas em Componentes. Conteúdo suficiente para você conseguir um emprego como desenvolvedor Web Fullstack e se tornar um desenvolvedor Web de sucesso.

# Conteúdo do Curso:

    - Seção 1: Introdução Curso Web (Concluído: 23/12/2019)
    - Seção 2: Configuração do Ambiente (Concluído: 23/12/2019)
    - Seção 3: Javascript: Fundamentos (Concluído: 24/12/2019)
    - Seção 4: Javascript: Estruturas de Controle
    - Seção 5: Javascript: Função
    - Seção 6: Javascript: Objeto
    - Seção 7: Javascript: Array
    - Seção 8: Node
    - Seção 9: ESNext
    - Seção 10: Conceitos sobre Web
    - Seção 11: HTML
    - Seção 12: CSS
    - Seção 13: Integrando HTML, CSS e JS
    - Seção 14: Ajax
    - Seção 15: Gulp
    - Seção 16: Webpack
    - Seção 17: jQuery
    - Seção 18: Bootstrap
    - Seção 19: Projeto Galeria (Bootstrap/jQuery/Webpack)
    - Seção 20: React
    - Seção 21: Projeto Calculadora
    - Seção 22: Projeto Cadastro de Usuário
    - Seção 23: VueJS
    - Seção 24: Projetos VueJS
    - Seção 25: Projeto Calculadora (Vue)
    - Seção 26: Projeto Monty Hall (Vue)
    - Seção 27: Banco Relacional
    - Seção 28: Banco Não Relacional
    - Seção 29: Express
    - Seção 30: Projeto Base de Conhecimento • Backend
    - Seção 31: Projeto Base de Conhecimento • Frontend
    - Seção 32: Publicando uma Aplicação VueJS na Amazon
    - Seção 33: Conclusão
